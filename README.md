# Actualize Web Client
A single page web application built with React Typescript. The web client is fully accessible, inclusive and WCAG 2.1 spec compliant thanks to the power of the [Grommet Framework](https://v2.grommet.io/).

# Prerequisites
- NodeJs 
- npm
- [actualize-orm](https://gitlab.com/actualize-social-goals/actualize-orm) module properly built
- [actualize-web-service](https://gitlab.com/actualize-social-goals/actualize-web-service) module built and running

# Installation
Clone the repository
```
git clone https://gitlab.com/Pensai/actualize-web-client.git
cd actualize-web-client
npm install
```
Start the application
`npm start`

Navigate to `http://localhost:3000` in your browser and verify that the stack is fully functioning by clicking on the `profile icon` on the top right, and clicking on the `sign up` button and filling out the form. The UI is currently in a rough state, so once you click `sign up` after filling out the form, simply refesh the page to see the newly added user.
 
# Notes
The front end utilizes the npm package [openapi-typescript-codegen](https://github.com/ferdikoomen/openapi-typescript-codegen) to read the web services OpenApi specification to automate generating types. These TypeScript types facilite proper object typing during development, and ensures that the front end is aware of any entity changes made in the back end.
In addition to typing, it also generates service classes to make interacting with the web service simple and cost of maintenance down when updates are made to the web service.
