const axios = require("axios")
const write = require("write");

async function fetchOpenApiSpec() {
    const noFetch = process.env.npm_config_nofetch;

    if(noFetch !== "true") {
        const configuredAxios = axios.create({
            baseURL: "http://localhost:1970/api",
            withCredentials: true,
            responseType: "json",
            auth: {
                username: "frontend",
                password: "frontend"
            },
            headers: {
                "Content-Type": "application/json"
            }
        });
        await configuredAxios.get('/v3/api-docs').then(r => {
            console.log(JSON.stringify(r.data));
            write.sync("spec.json", JSON.stringify(r.data), {overwrite: true});
        }).catch(e => console.log(e));
    } else {
        return;
    }

}
console.log()
fetchOpenApiSpec().then((returned) => {

} );
