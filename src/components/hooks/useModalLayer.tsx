import {useEffect, useState} from "react";

export function useModalLayer() {
    const [isVisible, toggleVisibility] = useState(false);

    const toggle = () => {
        toggleVisibility(!isVisible);
    }

    return {
        isVisible,
        toggle,
    }
};

export default useModalLayer;