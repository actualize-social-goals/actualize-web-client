import {Box, Button, Layer} from "grommet";
import {FormClose, Notification} from "grommet-icons";
import React from "react";

interface ISidebarProps {
    showSidebar: boolean

    setShowSidebar(isVisible: boolean): void
}

const Sidebar = (props: ISidebarProps) => {

    const toggleVisibility = () => {
        props.setShowSidebar(!props.showSidebar);
    }

    return (
        <Box>
            <Button icon={<Notification/>} onClick={toggleVisibility}/>
            {props.showSidebar &&
            <Layer responsive={true} full='vertical' position='left'>
                <Box
                    background='light-2'
                    tag='header'
                    direction='row'
                    align='center'
                    justify='end'>
                    <Button
                        icon={<FormClose/>}
                        onClick={toggleVisibility}
                    />
                </Box>

            </Layer>}
        </Box>
    )
}

export default Sidebar;
