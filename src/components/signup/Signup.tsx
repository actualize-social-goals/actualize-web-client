import React, {FunctionComponent, useState} from "react";
import {
  Button,
  CheckBox,
  FormExtendedEvent,
  FormField,
    TextInput,
} from "grommet";
import axios from "../../service/RestTemplate";
import {FormModal} from "../modal/FormModal";
import useModalLayer from "../hooks/useModalLayer";

interface FormState {
  username?: string;
  password?: string;
  email?: string;
  twoFactorAuth?: boolean
}

const formDefaults = {
  username: '',
  password: '',
  email: '',
  twoFactorAuth: false
};

export const Signup: FunctionComponent = () => {
  const {isVisible, toggle} = useModalLayer();
  const [value, setValue] = useState<FormState>(formDefaults);

  const onChange = (event: FormExtendedEvent<FormState>) => {
    setValue(event.value);
  }

  const handleSignup = async (event: FormExtendedEvent<FormState>) => {
     await axios.post("user/create", event.value)
      .then(r => {
        console.log(r)
      }).catch(e => console.log(e));
  }

  const signupForm = <>
        <FormField name='email' htmlFor='txt-email' label='Email' >
            <TextInput id='txt-email' name='email'/>
        </FormField>
        <FormField name='username' htmlFor='txt-username' label='Username' >
            <TextInput id='txt-username' name='username'/>
        </FormField>
        <FormField name='password' htmlFor='txt-password' label='Password'>
            <TextInput id='txt-password' name='password'/>
        </FormField>
      <FormField name='twoFactorAuth' >
          <CheckBox id="two-factor-auth" label='Use Two-Factor Authentication' name='twoFactorAuth'/>
      </FormField>
    </>;

  const formFooter = <>
    <Button type='submit' primary label='Sign up' />
  </>;

  return (
    <>
      <Button secondary label="Sign up" onClick={toggle}/>
      <FormModal toggle={toggle}
                 headerText="Sign up"
                 onChange={onChange}
                 onSubmit={handleSignup}
                 isShown={isVisible}
                 formContent={signupForm}
                 formFooter={formFooter}/>
    </>
  )
}