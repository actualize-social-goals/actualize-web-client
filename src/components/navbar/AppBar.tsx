import React from "react";
import { Heading, Nav, Button, Box } from 'grommet';

interface IAppbarProps {
  children?: React.ReactNode
}

const AppBar = (props: IAppbarProps) => (
  <Box
    tag='header'
    direction='row'
    align='center'
    justify='between'
    background='brand'
    pad={{left: 'medium', right: 'small', vertical: 'small'}}
    elevation='medium'
    {...props}
  >
    <Heading level='3' margin='none'>Actualize</Heading>
    {props.children}
  </Box>
);

export default AppBar;