import React, {FunctionComponent} from "react";
import {Box, Button, Form, FormExtendedEvent, Heading, Layer} from "grommet";
import {Close} from "grommet-icons";

export interface ModalProps {
    isShown: boolean;
    toggle: () => void;
    headerText: string;
    onChange: (nextValue: FormExtendedEvent<any>) => void;
    onSubmit: (value: FormExtendedEvent<any>) => void;
    formContent: JSX.Element;
    formFooter: JSX.Element;
}

export const FormModal: FunctionComponent<ModalProps> = ({
    isShown,
    toggle,
    headerText,
    onChange,
    onSubmit,
    formContent,
    formFooter,

 }) => {
    const modal = (
        <Layer onEsc={toggle}
               onClickOutside={toggle}>
            <Box overflow='auto'
                 width='medium'
                 pad='medium'>
                <Box flex={false} direction="row" justify="between">
                    <Heading level={2} margin="none">
                        {headerText}
                    </Heading>
                    <Button icon={<Close/>} onClick={toggle}/>
                </Box>
                <Box width="medium" pad={{vertical: 'medium'}}>
                    <Form onChange={onChange}
                          onSubmit={onSubmit}>
                        {formContent}
                        <Box as='footer' flex={false} direction='row' align='start'>
                            {formFooter}
                        </Box>
                    </Form>
                </Box>
            </Box>
        </Layer>
    );

    return isShown ? modal : null;
};