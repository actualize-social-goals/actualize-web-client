import React from "react";
import User from "../IEntity/User";
import axios from "../service/RestTemplate";

interface IProps {
}

interface IState {
  users: JSX.Element[];
}

class Users extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      users: []
    };
  }

  async componentDidMount() {
    let data = await axios.get("/user/users").then(r => {
      return r.data
    }).catch(e => console.log(e));

    if(data != null) {
      this.setState({
        users: (data).map(
          (u: User) =>
            <div>
              <p>Username: {u.username}</p>
              <p>Password: {u.password}</p>
            </div>
        )
      });
    }
  }

  render() {
    return (
      <div id='test'>
        {this.state.users}
      </div>
    )
  }
}

export default Users