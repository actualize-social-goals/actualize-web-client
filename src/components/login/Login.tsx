import React, {FunctionComponent, useState} from "react";
import {Box, Button, CheckBox, FormExtendedEvent, FormField, TextInput} from "grommet";
import {User} from "grommet-icons";
import useModalLayer from "../hooks/useModalLayer";
import {FormModal} from "../modal/FormModal";
import {Signup} from "../signup/Signup";


interface ILoginState {
    isLoggedIn: boolean
}

interface LoginProps {
    isLoggedIn: boolean
}

interface FormState {
    username: string;
    password: string;
}

const formDefaults = {
    username: '',
    password: ''
}

export const Login: FunctionComponent<LoginProps> = (ILoginState) => {
    const {isVisible, toggle} = useModalLayer();
    const [value, setValue] = useState<FormState>(formDefaults);

    const onChange = (event: FormExtendedEvent<FormState>) => {
        setValue(event.value);
    };

    const handleLogin = (event: FormExtendedEvent<FormState>) => {
        console.log(event);
    }

    const loginForm = <>
        <Box flex='grow' overflow='auto'>
            <FormField name='username' htmlFor='txt-user' label='Username'>
                <TextInput id='txt-username' name='username' />
            </FormField>
            <FormField name='password' htmlFor='txt-password' label='Password'>
                <TextInput id='txt-password' name='password' />
            </FormField>
            <CheckBox label='Remember me?' />
        </Box>
    </>;

    const formFooter = <>
        <Button type="submit"
                label="Login"
                primary/>
        <Signup />
    </>;

    return (
        <>
            <Button icon={<User/>} onClick={toggle}/>
            <FormModal toggle={toggle}
                       headerText='Login'
                       isShown={isVisible}
                       onChange={onChange}
                       onSubmit={handleLogin}
                       formContent={loginForm}
                       formFooter={formFooter}/>
        </>
    );
};

