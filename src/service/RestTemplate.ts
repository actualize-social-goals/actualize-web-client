import axios from 'axios';

export default axios.create({
    baseURL: "http://localhost:1970/api",
    withCredentials: true,
    responseType: "json",
    auth: {
        username: "frontend",
        password: "frontend"
    },
    headers: {
        "Content-Type": "application/json"
    }
});