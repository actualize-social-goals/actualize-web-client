import './App.css';
import Users from "../components/Users";
import React, { useState } from "react";
import {
  Grommet,
  Box,
  ResponsiveContext,
  Main, Nav
} from "grommet";
import AppBar from "../components/navbar/AppBar"
import Sidebar from "../components/sidebar/Sidebar";
import {Login} from "../components/login/Login";

const theme = {
  global: {
    font: {
      family: 'Roboto',
      size: '18px',
      height: '20px',

    },
    colors: {
      brand: '#63b175',
      text: {
        dark: 'white',
        light: 'black'
      },
      control: 'brand'
    },
    button: {
      color: {
        text: '#ffffff'
      }
    }
  }
};

const App = () => {
  const [showSidebar, setShowSidebar] = useState<boolean>(false);
  const [showLogin, setShowLogin] = useState<boolean>(false);


  return (
    <Grommet theme={theme} full>
      <ResponsiveContext.Consumer>
        {size => (
          <Main>
            <AppBar>
              <Nav direction='row'>
                <Sidebar showSidebar={showSidebar} setShowSidebar={setShowSidebar} />
                <Login isLoggedIn={false}/>
              </Nav>
            </AppBar>

            <Box flex align='center' justify='center'>
              <Users />
            </Box>


          </Main>
        )}
      </ResponsiveContext.Consumer>
    </Grommet>
  );
}

export default App;
