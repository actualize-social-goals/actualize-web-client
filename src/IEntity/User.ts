interface User {
  username: string
  password: string
  email: string
  twoFactorAuth: string
  createdBy?: string
  createdDate?: string
  updatedBy?: string
  updatedDate?: string
  userId?: number
  userProfiles?: object
}

export default User;